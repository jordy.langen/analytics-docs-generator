import 'package:analytics/analytics.dart';

const signInFlowFinishedKey = "sign_in_flow_finished";

void main() {
  logEvent(AnalyticsEvent event, [Map<String, Object>? properties]) {
    print("$event: $properties");
  }

  logEvent(
    // Sign in flow started event
    AnalyticsEvent("sign_in_flow_started"),
    // The component/screen which caused the sign in flow to be started
    {"origin": "settings"},
  );

  const successfulKey = "successful";
  Object? tokens;

  logEvent(
    // The sign in flow finished
    AnalyticsEvent(signInFlowFinishedKey),
    {
      // Whether the sign in was completed by the user and OAuth tokens could be retrieved
      successfulKey: "${tokens != null}",
      // Did we show the sign up button
      "sign_up_hint": true.toString(),
    },
  );
}
