import 'package:analytics_docs_generator/analytics_docs_generator.dart' as analytics_docs_generator;

Future<void> main(List<String> arguments) async {
  final generator = analytics_docs_generator.AnalyticsDocsGenerator();
  await generator.run(root: "/Users/jordylangen/develop/analytics-docs-poc/example/lib/src");
}
