import 'dart:io';

import 'package:analytics/analytics.dart';
import 'package:analyzer/dart/analysis/analysis_context_collection.dart';
import 'package:analyzer/dart/analysis/results.dart';
import 'package:analyzer/dart/ast/ast.dart';
import 'package:analyzer/dart/ast/visitor.dart';
import 'package:collection/collection.dart';

class AnalyticsDocsGenerator {
  Future<void> run({required String root}) async {
    final analysisContext = AnalysisContextCollection(
      includedPaths: [root],
    );

    final files = await Directory(root).list(recursive: true).toList();

    for (final file in files.where((element) => element.path.endsWith(".dart"))) {
      final context = analysisContext.contextFor(root);
      final library = await context.currentSession.getResolvedLibrary(file.path);

      if (library is ResolvedLibraryResult) {
        for (final resolvedUnit in library.units) {
          final visitor = _Visitor();
          resolvedUnit.unit.visitChildren(visitor);
        }
      } else {
        print("could not resolve ${file.path}");
      }
    }
  }
}

class _Visitor extends RecursiveAstVisitor<void> {
  @override
  void visitMethodInvocation(MethodInvocation node) {
    if (node.methodName.name == "logEvent") {
      print("\n");
      print("logEvent:");

      final analyticsEventArgument = node.argumentList.arguments
          .where(
            (argument) => argument.childEntities
                .whereType<ConstructorName>()
                .any((constructorName) => constructorName.type.name.name == "$AnalyticsEvent"),
          )
          .cast<InstanceCreationExpression>()
          .singleOrNull;

      final mapArguments = node.argumentList.arguments
          .where(
            (argument) => argument.childEntities.whereType<MapLiteralEntry>().isNotEmpty,
          )
          .cast<SetOrMapLiteral>()
          .singleOrNull;

      if (analyticsEventArgument != null) {
        // https://github.com/dart-lang/sdk/issues/37566
        print(analyticsEventArgument.beginToken.precedingComments ?? analyticsEventArgument.endToken.precedingComments);
        print(analyticsEventArgument);

        if (mapArguments != null) {
          for (final element in mapArguments.elements) {
            print(element.beginToken.precedingComments ?? mapArguments.beginToken.precedingComments);
            print(element);
          }
        }
      }
    }

    super.visitMethodInvocation(node);
  }
}
