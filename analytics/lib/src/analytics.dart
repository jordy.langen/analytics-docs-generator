typedef AnalyticsEventLogger = void Function(
  AnalyticsEvent, [
  Map<String, Object>? properties,
]);

enum AnalyticsAudience {
  all,
  sampleGroup,
}

enum AnalyticsPriority {
  essential,
  nonEssential,
}

class AnalyticsEvent {
  const AnalyticsEvent(
    this.name, {
    this.priority = AnalyticsPriority.essential,
    this.audience = AnalyticsAudience.all,
  });

  final String name;
  final AnalyticsPriority priority;
  final AnalyticsAudience audience;
}
